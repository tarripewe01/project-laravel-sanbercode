<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Form</title>
    <style>
        html,
        body {
            background-color: #16697A;
            color: #F8F1F1;
            font-family: 'Viaoda Libre', cursive;
            ;
            font-weight: 200;
            height: 100vh;
            
        }
        </style>
</head>

<body >
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>

    <form action="./welcome" method="POST">
        @csrf
        <label for="firstName">First Name:</label><br />
        <input type="text" id="firstName" name="firstName" value="" /><br /><br />
        <label for="lastName">Last Name:</label><br />
        <input type="text" id="lastName" name="lastName" value="" /><br /><br />
        <label for="gender">Gender:</label><br /><br />
        <input type="radio" id="male" name="gender" value="male" />
        <label for="male">Male</label><br />
        <input type="radio" id="female" name="gender" value="female" />
        <label for="female">Female</label><br />
        <input type="radio" id="other" name="gender" value="other" />
        <label for="other">Other</label><br /><br />
        <label>Nationality:</label><br /><br />
        <select id="nationality" name="nationality">
            <option value="indonesian">Indonesian</option>
            <option value="singaporean">Singaporean</option>
            <option value="malaysian">Malaysian</option>
            <option value="australian">Australian</option>
            <option value="others">Others</option>
        </select><br /><br />
        <label>Language Spoken:</label><br /><br />
        <input type="checkbox" id="bahasa1" name="bahasa1" value="Bahasa Indonesia" />
        <label for="bahasa1"> Bahasa Indonesia</label><br />
        <input type="checkbox" id="bahasa2" name="bahasa2" value="English" />
        <label for="bahasa2"> English</label><br />
        <input type="checkbox" id="bahasa3" name="bahasa3" value="Other" />
        <label for="bahasa3"> Other</label><br /><br />
        <label>Bio:</label><br /><br />
        <textarea id="bio" name="bio" rows="15" cols="50"> </textarea><br /><br />
        <input type="submit" value="Sign Up" />
    </form>
</body>

</html>