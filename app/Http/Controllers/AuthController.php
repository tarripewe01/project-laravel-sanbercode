<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{


    public function welcome()
    {
        return view('welcome');
    }
    
     public function welcome_post(Request $request)
    {
        $firstName = $request["firstName"];
        $lastName = $request["lastName"];
        return "Welcome $firstName  $lastName ";
    }

    public function register()
    {
        return view('register');
    }
}
